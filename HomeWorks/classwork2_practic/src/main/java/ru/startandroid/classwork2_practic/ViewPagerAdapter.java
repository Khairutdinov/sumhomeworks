package ru.startandroid.classwork2_practic;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Home on 10.07.2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private List<String> list;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        list = new ArrayList<>();
        list.add("sdgfgd");
        list.add("sdgfgwerwed");
        list.add("sdgfgd123123");
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new EventFragment();
            default:
                return new TestFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return list.get(position);
    }
}
