package ru.startandroid.classwork2_practic;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Home on 10.07.2017.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder>{

    List<Event> events;
    OnEventClickListener onEventClickListener;

    public EventAdapter(List<Event> events,OnEventClickListener onEventClickListener) {
        this.events = events;
        this.onEventClickListener = onEventClickListener;
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.event_fragment,parent,false));
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        holder.eventname.setText(events.get(position).getEventName());
        holder.description.setText(events.get(position).getDescription());
        //Picasso.with(holder.eventname.getContext())//не важно откуда брать контекст
        //        .load(events.get(position).getImage()).into(holder.imageView);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventClickListener.onEventClick();
            }
        });
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout relativeLayout;
        TextView eventname;
        TextView description;
        ImageView imageView;

        public EventViewHolder(View itemView) {
            super(itemView);
            eventname = (TextView) itemView.findViewById(R.id.event_name);
            description = (TextView) itemView.findViewById(R.id.text_description);
            imageView = (ImageView) itemView.findViewById(R.id.event_imege);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relative_layout);
        }
    }

    interface OnEventClickListener{
        public void onEventClick();
    }


}
