package ru.startandroid.classwork2_practic;

/**
 * Created by Home on 10.07.2017.
 */

public class Event {

    private String eventName;
    private String description;
    private String Image;



    public String getEventName() {
        return eventName;
    }

    public Event setEventName(String eventName) {
        this.eventName = eventName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Event setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getImage() {
        return Image;
    }

    public Event setImage(String image) {
        Image = image;
        return this;
    }
}
