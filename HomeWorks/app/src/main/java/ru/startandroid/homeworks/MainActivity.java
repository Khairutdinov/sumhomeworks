package ru.startandroid.homeworks;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView screen;

    Button clear;

    Button enter;

    LinearLayout linearLayout;

    TableLayout numPad;

    LinearLayout linearLayoutForBut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();

        setContentView(linearLayout);

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screen.setText("");
            }
        });

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(),screen.getText(),Toast.LENGTH_LONG);
                toast.show();
                screen.setText("");
            }
        });

    }

    private void init(){
        linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        ));

        screen = new TextView(this);
        numPad = new TableLayout(this);
        linearLayoutForBut = new LinearLayout(this);

        screen.setText("");

        byte columnNum = 3;

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1
        );
        layoutParams.setMargins(16,0,16,0);

        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                2
        );
        layoutParams1.setMargins(16,0,16,0);

        screen.setLayoutParams(layoutParams1);
        linearLayoutForBut.setOrientation(LinearLayout.HORIZONTAL);
        linearLayoutForBut.setLayoutParams(layoutParams1);

        numPad.setLayoutParams(layoutParams);

        TableLayout.LayoutParams tableLayPar = new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1
        );

        TableRow.LayoutParams tableRowLP = new TableRow.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1
        );
        int count = 1;
        for(int i = 0; i < columnNum; i++ ) {
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(tableLayPar);

            for (int j = 0; j < columnNum; j++) {
                final Button button = new Button(this);
                button.setLayoutParams(tableRowLP);
                final String te = count++ + "";
                button.setText(te);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        screen.setText(screen.getText() + "" + te);
                    }
                });
                tableRow.addView(button,j);
            }

            numPad.addView(tableRow,i);

        }

        LinearLayout.LayoutParams linlay = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1
        );
        clear = new Button(this);
        clear.setLayoutParams(linlay);
        clear.setText(R.string.clear);

        enter = new Button(this);
        enter.setLayoutParams(linlay);
        enter.setText(R.string.enter);

        linearLayoutForBut.addView(clear);
        linearLayoutForBut.addView(enter);

        linearLayout.addView(screen);
        linearLayout.addView(numPad);
        linearLayout.addView(linearLayoutForBut);
    }
}
