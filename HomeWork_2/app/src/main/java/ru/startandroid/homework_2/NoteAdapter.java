package ru.startandroid.homework_2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Home on 10.07.2017.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder>{

    List<Note> notes;

    NoteOnClickListener noteOnClickListener;

    public NoteAdapter(List<Note> notes,NoteOnClickListener noteOnClickListener){
        this.notes = notes;
        this.noteOnClickListener = noteOnClickListener;
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NoteViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.note_layout,parent,false)
        );
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, final int position) {
        holder.name.setText(notes.get(position).getName());
        holder.about.setText(notes.get(position).getAbout());
        holder.date.setText(notes.get(position).getDate());
        holder.difficulty.setText("Сложность:" + "" +notes.get(position).getDifficulty());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                noteOnClickListener.doSomething(notes.get(position).getMoreInformation());

            }
        });
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public static class NoteViewHolder extends RecyclerView.ViewHolder{

        private TextView name;
        private TextView about;
        private TextView difficulty;
        private TextView date;
        private RelativeLayout relativeLayout;

        public NoteViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.text_view_name);
            about = (TextView) itemView.findViewById(R.id.text_view_about);
            difficulty = (TextView) itemView.findViewById(R.id.text_view_difficulty);
            date = (TextView) itemView.findViewById(R.id.text_view_date);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relative_layout);
        }
    }

    public interface NoteOnClickListener{
        void doSomething(String text);
    }
}
