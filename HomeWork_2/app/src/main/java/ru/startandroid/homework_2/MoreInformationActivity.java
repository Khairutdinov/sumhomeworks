package ru.startandroid.homework_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MoreInformationActivity extends AppCompatActivity {

    public static final String KEY = "KEY";

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_information);

        textView = (TextView) findViewById(R.id.text_view_moreInformation);

        if(getIntent().getExtras()!=null){
            textView.setText(getIntent().getStringExtra(KEY));
        }

    }
}
