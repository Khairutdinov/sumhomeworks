package ru.startandroid.homework_2;

import java.io.Serializable;

/**
 * Created by Home on 10.07.2017.
 */

public class Note implements Serializable{

    private String name;
    private String about;
    private String difficulty;
    private String date;
    private String moreInformation;

    public String getName() {
        return name;
    }

    public Note setName(String name) {
        this.name = name;
        return this;
    }

    public String getAbout() {
        return about;
    }

    public Note setAbout(String about) {
        this.about = about;
        return this;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public Note setDifficulty(String difficulty) {
        this.difficulty = difficulty;
        return this;
    }

    public String getDate() {
        return date;
    }

    public Note setDate(String date) {
        this.date = date;
        return this;
    }

    public String getMoreInformation() {
        return moreInformation;
    }

    public Note setMoreInformation(String moreInformation) {
        this.moreInformation = moreInformation;
        return this;
    }
}
